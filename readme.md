## Bill Splitter

App for sharing the bills among friends.

Features supported

* Login
* Create bills based on category
* Exclude users from the bill
* View list of bills
* Summary based on month
* Setllement on every month

### Stack

Laravel framework, MySQL, PHP5.
