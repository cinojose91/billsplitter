@include('header')
@include('sidebar') 

<div class="content-wrapper"> <!--Main wrapper for the class-->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add New Bill

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add New Bill</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="box box-danger">
             {!! Form::open(array('url' => 'transaction/save','method'=>'post','files' => true)) !!}
            <div class="box-body">
                
                 @if ($errors->any())
                
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    <li> {{ $error }} </li>
                    @endforeach   
                </ul>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">{{ Session::get('error') }}</div>
                @endif
                 @if(Session::has('Success'))
                <div class="alert alert-success" role="alert">{{ Session::get('Success') }}</div>
                @endif
                <table id="CreateTable" class="table table-bordered table-hover">
                    <thead>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="col-sm-4">
                                Item Type:
                            </th>
                            <th class="col-sm-3">
                    
                                <select name="type" class="form-control col-sm-8" >
                                    @foreach($types as $type)
                                        <option value="{{ $type['id'] }}">{{ $type['name'] }}</option>
                                    @endforeach
                                    
                                </select>
                   
                            </th>
                        </tr>
                        <tr>
                            <th class="col-sm-4">
                                Item Amount:
                            </th>
                            <th class="col-sm-3">
                               <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <input type="text" class="form-control" name="amount">
                                    
                              </div>
                            </th>
                        </tr>
                          <tr>
                            <th class="col-sm-4">
                                Item Receipt:
                            </th>
                            <th class="col-sm-3">
                              {!! Form::file('file_upload') !!}
                            </th>
                          </tr>
                          <tr>
                            <th class="col-sm-4">
                                Item Excludes:
                            </th>
                            <th class="col-sm-3">
                            
     <div class="button-group">
        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="caret"></span></button>
<ul class="dropdown-menu">
    @foreach($users as $user)
      <li><a href="#" class="small" data-value="{{$user['id']}}" tabIndex="-1"><input  name="exc[]"  value ="{{$user['id']}}" type="checkbox"/>&nbsp;{{ $user['name'] }}</a></li>
    @endforeach
</ul>
  </div>

                            </th>
                          </tr>
                           <tr>
                            <th class="col-sm-4">
                                Item Remarks:
                            </th>
                            <th class="col-sm-3">
                              {!! Form::textarea('notes',null, ['size' => '20x3']) !!}
                            </th>
                          </tr>
                    </tbody>
                </table>
            </div>
            <div class='box-footer'>
                 {!! Form::submit('Add',['class' => 'btn btn-danger']); !!}
            </div>
             {!! Form::close() !!}
        </div>
    </section>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
@include('footer')
<script>
    var options = [];

$( '.dropdown-menu a' ).on( 'click', function( event ) {

   var $target = $( event.currentTarget ),
       val = $target.attr( 'data-value' ),
       $inp = $target.find( 'input' ),
       idx;

   if ( ( idx = options.indexOf( val ) ) > -1 ) {
      options.splice( idx, 1 );
      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
   } else {
      options.push( val );
      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
   }

   $( event.target ).blur();
      
   console.log( options );
   return false;
});
</script>