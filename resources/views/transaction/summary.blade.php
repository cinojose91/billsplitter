@include('header')
@include('sidebar')
<div class="content-wrapper"> <!--Main wrapper for the class-->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Total for this month

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View Bills</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="box box-danger">
            <div class="box-body">
            
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        User Name
                    </th>
                    <th>
                          Total to pay
                    </th>
                    <th>
                          Amount I spent
                    </th>
                    
                </tr>
                
                </thead>
                <tbody>
                    @foreach($summary as $user)
                    <tr>
                        <td> {{ $user['name'] }}</td>
                        <td> {{ $user['total'] }}</td>
                        <td> {{$user['total'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </section>
</div>
@include('footer')