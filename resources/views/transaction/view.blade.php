@include('header')
@include('sidebar')
<div class="content-wrapper"> <!--Main wrapper for the class-->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View Bills

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View Bills</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-danger">
            <div class="box-body">
                <h4>Current Total : ${{$total}}</h4>
                <h4>Current Average : ${{$avg}}</h4>
                <table class="table table-bordered" id="mytable">
                    <thead>
                        <tr>
                            <th>
                                Bill No.
                            </th>
                            <th>
                                User Name
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                Amount
                            </th>
                            <th>
                                Date
                            </th>
                            <th> Image</th>

                        </tr>

                    </thead>
                    <tbody>
                        @foreach($details as $detail)
                        <tr>
                            <td> <a href="#"  onclick="loadTransaction({{ $detail['id'] }});return false;" >{{ $detail['id'] }} </a></td>
                            <td> {{ $detail['name'] }}</td>
                            <td> {{ $detail['type'] }}</td>
                            <td> ${{ $detail['amount'] }}</td>
                            <td> {{ $detail['date'] }}</td>
                            <td> @if($detail['receipt']!="")

                                <a href="/images/{{$detail['id']}}"><i class="fa fa-instagram"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Transaction Details</h4>
            </div>
            <div class="modal-body">
                <div id ="transact_data"></div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>
@include('footer')
<script>
    $(function () {
        $('#mytable').dataTable();
    });

    function loadTransaction(id) {
        $.ajax({
            type: "GET",
            url: "/transaction/get/"+id,
            success: function (msg)
            {
                data = processResponse(msg);
                $("#transact_data").html(data);
                $("#myModal").modal('show');
                
            }
        });

    }
    function processResponse(msg){
        var jsondata = JSON.parse(msg);
        var data = "<table class='table table-bordered'>";
        data += "<tr><th>Added by</th><td>"+jsondata[0].owner+"</td></tr>";
        data += "<tr><th>Amount(SGD) </th><td>"+jsondata[0].amount+"</td></tr>";
        data += "<tr><th>Bill type</th><td>"+jsondata[0].type+"</td></tr>";
        data += "<tr><th>Remarks</th><td>"+jsondata[0].remarks+"</td></tr>";
        excludes = "";
        for(i =0; i < jsondata.length; i++){
             excludes += jsondata[i].exludes +",";   
        }
        excludes = excludes.substring(0, excludes.length - 1);
        data += "<tr><th>Excludes</th><td>"+excludes+"</td></tr>";
        data += "<tr><th>Created at</th><td>"+jsondata[0].created_at+"</td></tr>";
        data += "</table>";
        return data;
    }
</script>