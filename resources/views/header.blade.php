<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>BSP | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    {!! Html::style('bootstrap/css/bootstrap.min.css') !!}
    <!-- FontAwesome 4.3.0 -->
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
    <!-- Ionicons 2.0.0 -->
    {!! Html::style('https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css') !!}   
    <!-- Theme style -->
    {!! Html::style('dist/css/AdminLTE.min.css') !!}
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    {!! Html::style('dist/css/skins/_all-skins.min.css') !!}
    <!-- iCheck -->
    {!! Html::style('plugins/iCheck/flat/blue.css') !!}
    <!-- Morris chart -->
    {!! Html::style('plugins/morris/morris.css') !!}
    <!-- jvectormap -->
    {!! Html::style('plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
    <!-- Date Picker -->
    {!! Html::style('plugins/datepicker/datepicker3.css') !!}
    <!-- Daterange picker -->
    {!! Html::style('plugins/daterangepicker/daterangepicker-bs3.css') !!}
    <!-- bootstrap wysihtml5 - text editor -->
    {!! Html::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <a href="../../index2.html" class="logo"><b>BillSplitter</b>788E</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                   
                  @if($count>0)
                    <span class="label label-danger">{{$count}}</span>
                  @endif
                </a>
                <ul class="dropdown-menu">
                  @if($count>0)
                        <li class="header">You have {{$count}} notifications</li>
                    @else
                        <li class="header">You have no notifications</li>
                    @endif
                  
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                        @foreach($notifications as $noti)
                            <li>
                                <a  id="notif" href="#" data-message = "{{$noti['noti_id']}}" onclick="changeNotiStatus({{$noti['id']}})" >
                                  @if($noti['type']=='Grocerry')
                                    <i class="fa fa-coffee text-aqua"></i> 
                                  @elseif($noti['type']=='PUB')
                                    <i class="fa fa-dribbble  text-warning"></i> 
                                  @elseif($noti['type']=='Singtel')
                                    <i class="fa fa-mobile text-warning"></i> 
                                  @elseif($noti['type']=='Maid')
                                    <i class="fa fa-clipboard text-danger"></i>
                                  @elseif($noti['type']=='Birthday')
                                    <i class="fa fa-bitcoin text-danger"></i>
                                   @elseif($noti['type']=='Others')
                                    <i class="fa fa-adjust text-danger"></i>
                                  @endif
                                  {{$noti['name']." Added a new bill"}}
                                </a>
                           </li>
                        @endforeach
                    </ul>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
             
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"> {{ Auth::user()->user_name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    <p>
                      {{ Auth::user()->name }}
                      <small>Member since {{ \Carbon\Carbon::createFromTimeStamp(strtotime(Auth::user()->updated_at))->toFormattedDateString() }}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
