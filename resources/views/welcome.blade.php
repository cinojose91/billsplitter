@include('header')
@include('sidebar') 

<div class="content-wrapper"> <!--Main wrapper for the class-->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Welcome {{ Auth::user()->name }}

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashbard</li>
        </ol>
        </section>
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="h4">Transaction history</h4>
            </div>
            <div class="box-body" id ="transact_stat" style="height: 300px;">
                
            </div>
            <div class="box-footer">...</div>
            
        </div>
    </section>
    
</div>
@include('footer')
<script>
  /*
 * Play with this code and it'll update in the panel opposite.
 *
 * Why not try some of the options above?
 */

 <?php
 $chartdata = "";
        foreach($data as $k=>$v){
            $label=$v['year']."(".$v['month'].")";
            $chartdata.="{y: "."'".$label."',a:".$v['total']."},";
        }
$chartdata = substr($chartdata, 0, -1);
        ?>
Morris.Bar({
      element: 'transact_stat',
      resize: true,
      data: [
        <?php echo $chartdata ?>
      ],
      barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['$'],
      hideHover: 'auto'
    });
</script>
