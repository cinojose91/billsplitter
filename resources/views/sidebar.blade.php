

<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p> {{ Auth::user()->name }}</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
              
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Transactions</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/transaction/add"><i class="fa fa-circle-o"></i> Add New</a></li>
                <li><a href="/transaction/view"><i class="fa fa-circle-o"></i> View transaction</a></li>
                <li><a href="/transaction/summary"><i class="fa fa-circle-o"></i> Summary</a></li>
                <li><a href="/transaction/mytransact"><i class="fa fa-circle-o"></i> My transaction</a></li>
                 <li><a href="/settlement/past"><i class="fa fa-circle-o"></i> Previous settlements</a></li>
                
              </ul>
            </li>
             <li class="treeview">
              <a href="/auth/logout">
                <i class="fa fa-dashboard"></i> <span>Logout</span>
              </a>
              @if(\Auth::user()->role==1)
              <li class="treeview">
              <a href="/settlement/view">
                <i class="fa fa-stack-overflow"></i> <span>Settlement</span>
              </a>
              
            </li>
            @endif
            </li>
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
