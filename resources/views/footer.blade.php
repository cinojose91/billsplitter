

</div>

     <!-- jQuery 2.1.3 -->
    {!! Html::script('plugins/jQuery/jQuery-2.1.3.min.js') !!}
   
    <!-- jQuery UI 1.11.2 -->
    {!! Html::script('https://code.jquery.com/ui/1.11.2/jquery-ui.min.js') !!}
    
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    {!! Html::script('bootstrap/js/bootstrap.min.js') !!}
    <!-- Morris.js charts -->
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!}
    <!--  Data tables -->
    {!! Html::script('plugins/datatables/jquery.dataTables.js') !!}
     {!! Html::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <!-- Sparkline -->
    {!! Html::script('plugins/sparkline/jquery.sparkline.min.js') !!}
    <!-- jvectormap -->
    {!! Html::script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    <!-- jQuery Knob Chart -->
    {!! Html::script('plugins/knob/jquery.knob.js') !!}
    <!-- daterangepicker -->
    {!! Html::script('plugins/daterangepicker/daterangepicker.js') !!}
    <!-- datepicker -->
    {!! Html::script('plugins/datepicker/bootstrap-datepicker.js') !!}
    <!-- Bootstrap WYSIHTML5 -->
    {!! Html::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
    <!-- iCheck -->
    {!! Html::script('plugins/iCheck/icheck.min.js') !!}
    <!-- Slimscroll -->
    {!! Html::script('plugins/slimScroll/jquery.slimscroll.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('plugins/fastclick/fastclick.min.js') !!}
    
    {!! Html::script('plugins/morris/morris.min.js') !!}
    <!-- AdminLTE App -->
    {!! Html::script('dist/js/app.min.js') !!}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
     {!! Html::script('js/pages/dashboard.js') !!}
    <!-- AdminLTE for demo purposes -->
    
  </body>
</html>

<script>
        
        function changeNotiStatus(id){
            //alert("Changing status of id"+id);
            $.ajax({
            type: "POST",
            data: {noti_id:id},
            url: "/notification/change",
            beforeSend: function( xhr ) {
             //$("#feedbox").html("<div class='box-body'></div><div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");
            }
          })
            .done(function( data ) {
              //$("#feedfooter").html("<span class='fa fa-clock-o'></span> last updated at {{ Carbon\Carbon::now()->format('d-M-Y:h:i:s') }}");
              //$("#feedbox").html(data);

            });
        }
  </script>