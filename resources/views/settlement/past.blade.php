@include('header')
@include('sidebar')
<div class="content-wrapper"> <!--Main wrapper for the class-->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            View settlements

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View Bills</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="box box-danger">
            <div class="box-body">
            
           <table id="auditTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>
                        User Name
                    </th>
                    <th>
                        Month
                    </th>
                    <th>
                          Amount
                    </th>
                    <th>
                          Settled Date
                    </th>
                    <th>
                          Year
                    </th>
                </tr>
                
                </thead>
                <tbody>
                    @foreach($settlements as $detail)
                    <tr>
                        <td> {{ $detail['name'] }}</td>
                        <td> {{ $detail['month'] }}</td>
                         <td> {{ $detail['amount'] }}</td>
                         <td> ${{ $detail['set_date'] }}</td>
                         <td> {{ $detail['year'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </section>
</div>
@include('footer')

<script>
      $(function () {
        $('#auditTable').dataTable();
    });
</script>
