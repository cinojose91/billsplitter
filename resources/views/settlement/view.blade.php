@include('header')
@include('sidebar')
<div class="content-wrapper"> <!--Main wrapper for the class-->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Do Settlement

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View Bills</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="box box-danger">
            <div class="box-body">
                 {!! Form::open(array('url' => 'settlement/dosettlement','method'=>'post')) !!} 
                    {!! Form::submit('Do settlement',['class' => 'btn btn-danger']); !!}
                 {!! Form::close() !!}
            </div>
        </div>
    </section>
</div>
@include('footer')

