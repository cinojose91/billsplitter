<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BillType extends Model {

	//
    protected $table = 'billtype';
    protected $primaryKey = 'id';

}
