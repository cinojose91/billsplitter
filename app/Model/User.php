<?php namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Log;
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
        
        protected $primaryKey = 'login_id';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'user_loginID', 'user_img' , 'user_name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
        
        /**
        * Get the unique identifier for the user.
        *
        * @return mixed
        */
       public function getAuthIdentifier()
       {
           return $this->login_id;
       }

       /**
        * Get the password for the user.
        *
        * @return string
        */
       public function getAuthPassword()
       {
           return $this->password;
       }

       /**
        * Get the e-mail address where password reminders are sent.
        *
        * @return string
        */
       public function getReminderEmail()
       {
           return $this->email;
       }
       
       public function setPasswordAttribute($password)
        {
            $this->attributes['password'] = bcrypt($password);
        } 

}
