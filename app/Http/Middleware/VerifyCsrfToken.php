<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
            $skip = array(
            'fetchgroup',
            'user/profile',
            'my/unprotected/route',
            'file/comment',
            'file/view/comments',
            //'file/search',
            'file/subscribe',
            'file/lock',
            'file/getdetails',
            'file/view/getdetails',
            'notification/change'
        );

        foreach ($skip as $key => $route) {
            //skip csrf check on route
            if ($request->is($route)) {
                return parent::addCookieToResponse($request, $next($request));
            }
        }
            
            return parent::handle($request, $next);
	}

}
