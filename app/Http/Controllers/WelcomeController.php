<?php namespace App\Http\Controllers;
    
use App\Model\TransactionStat;
use Illuminate\Support\Collection;
use Hash;
class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
                
	{
            
            $this->middleware('auth');// will redirect if the user is not logged in.
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
           
            $result = TransactionStat::all();
            return view('welcome')->with("data",$result);
	}

}
