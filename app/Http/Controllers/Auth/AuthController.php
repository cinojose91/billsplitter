<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\LoginRequest;    //login request class
use App\Model\User;
use AuditTrail;
use DB;
use Hash;
use Request;

use Log;
class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}
        
        public function ShowLoginForm(){
            //Log::info("reached login");    //This is how we can log.
            return view('auth.login');
        }
        
        /**
         *  Function to do the login action
         */
        public function LoginPost( LoginRequest $request ) {
            //dd(Hash::make('1234567'));
            $credentials = array('login_id' => $request->input('user_loginID'), 'password' => $request->input('password'));
            //Log::info(print_r($request->credentials()));
            if ( $this->auth->attempt( $credentials )) {
                //here need to do the audit trial
                //$this->addAuditTrail();
                //dd(\Auth::user()->name);
                return Redirect('/');
            }
            
            Log::info("Login unsucccessful");
            return Redirect( 'login' )->withErrors( [
                        'email' => 'Invalid User name or password' ,
                    ] );
        }
        
   
        public function addAuditTrail() {
            
            $atrail = new \App\Model\AuditTrail();
            $atrail->audit_action = 'Login';
            $atrail->user_ip=Request::getClientIp();
            $atrail->audit_user = \Auth::user()->user_loginID;
            $atrail->save();
            
        }
        
        
}


