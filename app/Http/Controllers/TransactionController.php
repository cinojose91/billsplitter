<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use App\Model\BillType;
use Redirect;
use App\Model\User;
use Carbon\Carbon;
use Storage;
use App\Http\Requests\AddRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use App\Model\Transaction;
use App\Model\TransactionUsers;
use DB;
use App\Model\Settings;
use App\Model\Settlement;

use App\Model\Notification;
use App\Model\NotificationUser;

class TransactionController extends Controller {

	
    
    
        public function __construct()
	{
		$this->middleware('auth');// will redirect if the user is not logged in.
	}
        
        /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
            $types = BillType::all();
            $users = user::all();
            $data = array("types"=>$types,"users"=>$users);
            return view('transaction.add')->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		
                $v = Validator::make($request::all(), [
                    'amount' => 'required|numeric',
                ]);

                if ($v->fails())
                {
                    return redirect()->back()->withErrors($v->errors());
                }



                //dd($request);
                $exclist = $request::input('exc');
                $file = "";
                
                
                if(sizeof($exclist)==8){
                    
                   return Redirect::back()->with('error', "Don't play play ah boy..!!"); 
                }
                else{
                    if($request::hasFile('file_upload')){
                        $disk = Storage::disk('local');
                        $upfile = $request::file('file_upload');
                        $datetime = Carbon::now()->format("mdyhis");
                        //dd($result['salt']);
                        $year = "Y".Carbon::now()->format("Y");
                        if(!file_exists($disk->exists($year))){
                            $disk->makeDirectory($year);
                        }
                        $month = Carbon::now()->format("m");
                        if(!file_exists($disk->exists($year."/".$month))){
                            $disk->makeDirectory($year."/".$month);
                        }
                        $day = Carbon::now()->format("d");
                        if(!file_exists($disk->exists($year."/".$month."/".$day))){
                            $disk->makeDirectory($year."/".$month."/".$day);
                        }
                        $dest = $year."/".$month."/".$day."/".$datetime.$upfile->getClientOriginalName();
                        Storage::disk('local')->put("receipt/".$dest,  File::get($upfile));
                        $file = "receipt/".$dest;
                    }
                    $t = new Transaction();
                    $t->user_id = \Auth::user()->id;
                    $t->type = $request::input('type');
                    $t->amount = $request::input('amount');
                    $t->file = $file;
                    $t->remarks = $request::input('notes');
                    $t->save();
                    if(sizeof($exclist)>0){
                        $transid = DB::getPdo()->lastInsertId();
                        $this->AddExclusion($transid, $exclist);
                    }
                    //doing notification
                    $n = new Notification();
                    $n->type = $request::input('type');
                    $n->user_id = \Auth::user()->id;
                    $n->save();
                    $notid = DB::getPdo()->lastInsertId();
                    $users = User::all();
                    foreach($users as $user ){
                        if(sizeof($exclist)==0){
                            if($user['id']==\Auth::user()->id){
                                continue;
                            }
                            $this->addNotification($notid, $user['id']);
                            continue;
                        }
                        if(in_array($user['id'], $exclist)){
                            continue;
                        }
                        else{
                            $this->addNotification($notid, $user['id']);
                        }
                    }
                    $message = "Bill added";
                    
                    return Redirect('/transaction/add')->with('Success',$message);
                }
                //dd(sizeof($exclist));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
            $settings = Settings::select('last_settlement')->get()->first();
            $last_settlement = $settings['last_settlement'];
            $details = Transaction::join('users','users.id','=','transaction.user_id')
                                   ->join('billtype','transaction.type','=','billtype.id')
                                   ->where('transaction.created_at','>',$last_settlement)->select("users.name AS name","billtype.name AS type","transaction.created_at AS date","transaction.amount","transaction.id AS id","transaction.file AS receipt")->get();
            
            $total = 0;
            foreach($details as $detail){
                $total = $total + $detail['amount'];
            }
            $avg = $total /8;
            $data = array("details"=>$details,"avg"=>$avg,"total"=>$total);
            return view('transaction.view')->with($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            //
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        
        /*
         * Add Exclusion list
         */
        public function AddExclusion($tran,$list){
            
            foreach($list as $item){
                $tu = new TransactionUsers();
                $tu->trans_id = $tran;
                $tu->user_id = $item;
                $tu->save();
            }
        }
        
        /*
         * function to view the present summary.
         */
        public function viewSummary(){
            $users = $this->getSummary();
            $data = array("summary"=>$users);
            return view('transaction.summary')->with($data);
        }
        
        /*
         * function to get exclusion list
         */
        public function getExclusionList($transid){
            $result = TransactionUsers::where('trans_id','=',$transid)->select("user_id")->get();
            return $result;
        }
        
        /*
         * function to check if user in exclusion list
         */
        public function CheckIfUserinList($userid,$exlist){
            $flag = false;
            foreach($exlist as $user){
                if($user['user_id'] == $userid){
                    $flag = true;
                }
            }
            return $flag;
        }
        
        /*
         * function to get my detail
         */
        public function myTransact(){
            
            $settings = Settings::select('last_settlement')->get()->first();
            $last_settlement = $settings['last_settlement'];
            $details = Transaction::join('users','users.id','=','transaction.user_id')
                                   ->join('billtype','transaction.type','=','billtype.id')
                                   ->where('transaction.user_id','=',\Auth::user()->id)
                                   ->where('transaction.created_at','>',$last_settlement)->select("users.name AS name","billtype.name AS type","transaction.created_at AS date","transaction.amount","transaction.id AS id")->get();
            
            $total = 0;
            foreach($details as $detail){
                $total = $total + $detail['amount'];
            }
            $avg = $total /8;
            $data = array("details"=>$details,"avg"=>$avg,"total"=>$total);
            return view('transaction.view')->with($data);
            
        }
        
        /*
         * function to view settlment page
         */
        
        public function View(){
            
            return view('settlement.view');
            
        }
        
        /*
         * Function to do the settlement
         */
        public function doSettlement(){
            $users = $this->getSummary();
            $month = Carbon::now()->format("M");
            $year = Carbon::now()->format("Y");
            $lastsettlement = Carbon::now()->toDateTimeString();
            foreach($users as $user){
                $st = new Settlement();
                $st->month = $month;
                $st->year = $year;
                $st->set_date = $lastsettlement;
                $st->user_id = $user['id'];
                $st->amount = round($user['total'],2);
                $st->save();
            }
            //TODO : Update the settings.
            $result = Settings::where('id', 1)->update(['last_settlement' => $lastsettlement]);
            dd("Completed Settlement for month".$month);
            
        }
        
        public function getSummary(){
            $users = User::all();
            
            //dd(sizeof($users));
            $settings = Settings::select('last_settlement')->get()->first();
            $last_settlement = $settings['last_settlement'];
            $details = Transaction::join('users','users.id','=','transaction.user_id')
                                   ->join('billtype','transaction.type','=','billtype.id')
                                   ->where('transaction.created_at','>',$last_settlement)->select("transaction.user_id AS userid","users.name AS name","billtype.name AS type","transaction.created_at AS date","transaction.amount","transaction.id AS id")->get();
            
            $total = 0;
            foreach($users as $user){
                $user['total'] = $user['base'];
            }
            $totalusers = sizeof($users);
            foreach($details as $detail){
                $exlist = $this->getExclusionList($detail['id']);
                $usertosplit = $totalusers - sizeof($exlist);
                $smamount = $detail['amount'] / $usertosplit;
                foreach($users as $user){
                    if($this->CheckIfUserinList($user['id'], $exlist)){
                        continue;
                    }elseif($user['id'] == $detail['userid']){
                        $myamount = $detail['amount'] - $smamount;
                        $user['total'] = $user['total'] - $myamount;
                    }
                    else{
                        $user['total'] = $user['total']+ $smamount;
                    }
                }
                
            }
            $rent = 0;
            foreach($users as $user){
                $rent = $rent + $user['total'];
            }
            
            
            return $users;
        }
        
        /*
         * Function to show the settlements
         */
        
        public function showSettlements(){
            $settlements = Settlement::join("users","users.id",'=','settlement.user_id')
                                      ->select("*")->get();
            $data = array("settlements"=>$settlements);
            
            return view('settlement.past')->with($data);
            
        }
        
        /*
         * Function to add Notification
         */
        public function addNotification($notid,$user){
            
            $nu = new NotificationUser();
            $nu->noti_id = $notid;
            $nu->user_id = $user;
            $nu->status = 0;
            $nu->save();
        }
        
        /*
         * function to retrive the notification
         */
        public function getMyNotifications($userid){
            $result = NotificationUser::join('notification as e','notificationuser.noti_id','=','e.id')
                                    ->join('users','e.user_id','=','users.id')
                                    ->join('billtype','billtype.id','=','e.type')
                                    ->where('notificationuser.user_id','=',$userid)
                                    ->where('notificationuser.status','=','0')
                                    ->orderBy('notificationuser.created_at', 'desc')
                                    ->select('billtype.name as type','users.name as name','notificationuser.id as id')
                                    ->get();
            return $result;
        }
        
        /*
         * function to change the noti status
         */
        
        public function ChangeStatus(Request $request) {
            
            $id = $request::input('noti_id');
            $noti = NotificationUser::find($id);
            $noti->status ='1';
            $noti->save();
        }
        
        /*
         * Function to return the image
         *
         */
        public function getImage($id){
            $t = Transaction::where("transaction.id",'=',$id)->select("file")->get();
            return $t;
        }
        
        
        /*
         * Function to return the transaction particular data
         */
        public function getTransacttion($id){
            $result = Transaction::leftJoin("transactionusers as tru","transaction.id","=","tru.trans_id")
                                   ->leftJoin("users as u1", "transaction.user_id","=","u1.id")
                                   ->leftJoin("billtype","transaction.type","=","billtype.id")
                                   ->leftJoin("users as u2", "tru.user_id","=","u2.id")
                                   ->where("transaction.id","=",$id)
                                   ->select("transaction.amount","transaction.file","transaction.remarks","u1.name as owner","billtype.name as type","u2.name as exludes", "transaction.created_at")
                                   ->get();
            return $result->toJson();
        }
}
