<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');
/*
 * Login
 */
Route::get('login','Auth\AuthController@ShowLoginForm');
Route::post('loginProc','Auth\AuthController@LoginPost');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*
 * Transactions
 */
Route::get('transaction/add','TransactionController@index');
Route::post('transaction/save','TransactionController@create');
Route::get('transaction/view','TransactionController@show');
Route::get('transaction/summary','TransactionController@viewSummary');
Route::get('transaction/mytransact','TransactionController@myTransact');
Route::get('transaction/get/{id}', 'TransactionController@getTransacttion');

/*
 * Do settlement
 */
Route::get('settlement/view','TransactionController@View');
Route::post('settlement/dosettlement','TransactionController@doSettlement');
Route::any('settlement/past','TransactionController@showSettlements');

Route::any('/notification/change','TransactionController@ChangeStatus');

Route::any('images/{filename}', function ($filename)
{
    //dd($filename);
    $filed = App::make('App\Http\Controllers\TransactionController')->getImage($filename);
    $path = storage_path() . '/app/' . $filed[0]->file;
    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
/*
 * View Composers for loading notifications.
 */
View::composer('header', function($view)
{
  $userid = \Auth::user()->id;
  $notifications = $rule = App::make('App\Http\Controllers\TransactionController')->getMyNotifications($userid);
  $notcount = sizeof($notifications);
  $data = array('notifications'=>$notifications,'count'=>$notcount);
  //dd($data);
  $view->with($data);
});



