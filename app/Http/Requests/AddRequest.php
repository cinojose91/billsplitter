<?php 
// LoginRequest 
namespace App\Http\Requests;
 
use Illuminate\Foundation\Http\FormRequest;
 
class AddRequest extends FormRequest {
    public function rules() {
        
        return [
            'amount' => 'required' ,
        ];
    }
 
    public function remember() {
        return $this->has( 'remember' );
    }
    
    public function credentials() {
        return $this->only( 'login_id' , 'password' );
    }
 
    public function authorize() {
        return true;
    }
}
