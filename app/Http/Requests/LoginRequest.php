<?php 
// LoginRequest 
namespace App\Http\Requests;
 
use Illuminate\Foundation\Http\FormRequest;
 
class LoginRequest extends FormRequest {
    public function rules() {
        return [
            'user_loginID' => 'required|min:6' ,
            'password' => 'required|min:6' ,
        ];
    }
 
    public function remember() {
        return $this->has( 'remember' );
    }
    
    public function credentials() {
        return $this->only( 'login_id' , 'password' );
    }
 
    public function authorize() {
        return true;
    }
}
